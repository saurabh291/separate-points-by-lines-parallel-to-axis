#Separating points through Greedy Approach
from itertools import combinations
from os import walk, mkdir

def separator(co_ordinates, co_ordinates_Y, comb, X, Y):

    map_X = []
    map_Y = []
    final_val = []

	#Checking whether the list is empty and all points are separated
    while len(comb) != 0:				
        #print(len(comb))
        max_splits = 0
        temp_md_x = []
		#Making all possible intercepts 
        for idx_i, val in enumerate(co_ordinates):  
            if idx_i < len(co_ordinates) - 1:
                md_x = (X[idx_i] + X[idx_i + 1]) / 2
                temp_md_x.append(md_x)
                splits_X = 0
				# Checking the number of pairs which will split if we make vertical lines at a intercept
                for idx_j, cmb in enumerate(comb):
                    # Calculating no. of split
                    if cmb[0][0] < md_x and cmb[1][0] > md_x:
                        #print("Checking with", cmb[0][0], cmb[1][0]," ", end="")
                        splits_X += 1
                        #print(splits_X)
                if splits_X > max_splits:
                    max_splits = splits_X
                    map_X = idx_i
        # print("Map_X val", max_splits, temp_md_x[map_X])
        splits_X = map_X

        max_splits = 0
        temp_md_y = []
		#Making all possible intercepts 
        for idx_i, val in enumerate(co_ordinates_Y):
            #print("Checking for", val)
            if idx_i < len(co_ordinates_Y) - 1:
                md_y = (Y[idx_i] + Y[idx_i + 1]) / 2
                temp_md_y.append(md_y)
                splits_Y = 0
				# Checking the number of pairs which will split if we make horizontal lines at a intercept
                for idx_j, cmb in enumerate(comb):
                    # Calculating no. of split
                    if (cmb[0][1] < md_y and cmb[1][1] > md_y) or (cmb[1][1] < md_y and cmb[0][1] > md_y):
                        #print("Checking with", cmb[0], cmb[1]," ", end="")
                        splits_Y += 1

                if splits_Y > max_splits:
                    max_splits = splits_Y
                    map_Y = idx_i

        splits_Y = map_Y
        #print(splits_X, splits_Y)
		
		#Checking whether horizontal lines or vertical line gives us more points which will split 
		#If they are equal, we are chosing a vertical line
        if splits_X >= splits_Y:
            val_x = temp_md_x[map_X]

            comb_remove = []
            for indx, values in enumerate(comb):
                #print("Here", values[0][0], values[1][0])
                if values[0][0] < val_x and values[1][0] > val_x:
                    comb_remove.append(comb[indx])
                    #comb.pop(indx)
                    #print(comb)

            #print("Line Vertical: ",val_x)
            final_val.append("v "+str(val_x))

        else:
            #print("split on Y")
            comb_remove = []
            for indx,values in enumerate(comb):
                val_y = temp_md_y[map_Y]
                if (values[0][1] < val_y and values[1][1] > val_y) or (values[1][1] < val_y and values[0][1] > val_y):
                    comb_remove.append(comb[indx])
            #print("Line Horizontal: ",val_y)
            final_val.append("h "+str(val_y))
		
		#Removing the pairs from the original combination listCr
        for rem in comb_remove:
            comb.remove(rem)

    return final_val

#Crawling through a directory where we have inputs and taking all files which have points
def crawl_dir():

    input_files = []
    for (dirpath, dirnames, filenames) in walk("input"):
        input_files.extend(filenames)
        break
    return input_files

#Reading data from the input file
def read_input(file):
    with open("input/"+file,'r') as fp:
        cor_values = fp.read()
    co_ordinates = []
    cor_values = cor_values.split("\n")
    for c in cor_values[1:len(cor_values)-1]:
        X_c, Y_c = c.split(" ")
        X_c, Y_c = int(X_c), int(Y_c)
        co_ordinates.append((X_c,Y_c))

    return co_ordinates


if __name__ == "__main__":
    #Making a folder named as output greedy where our output files will be get stored
    mkdir("output_greedy")
    filenames = crawl_dir()
    for file in filenames:
        co_ordinates = read_input(file)
        #break
        co_ordinates_Y = sorted(co_ordinates, key= lambda y:y[1])
        #print(co_ordinates_Y)
        comb = []
        for val in combinations(co_ordinates,2):
            comb.append(val)
        #print(comb)

        X = [ele[0] for ele in co_ordinates]
        Y = sorted([ele[1] for ele in co_ordinates])


        result = separator(co_ordinates, co_ordinates_Y, comb, X, Y)

		#Naming the output files taking e.g. 00, 01 etc from input file and adding it to output file name
        with open("output_greedy/greedy_solution"+file[8:10],"w+") as fp:
            fp.write(str(len(result)))
            fp.write("\n")
            for sep in result:
                fp.write(sep)
                fp.write("\n")
    print("Output generated in sub dir named output_greedy")